a = "Hello world!"

a[0]           # 1st character of the string
len(a)         # Size of the string
a.index('o')   # Find the position of 'o'
a.upper()      # All in capital letters
a.lower()      # All in lowercase letters
'-' * 10       # Result: "----------"
'abc' + 'def'  # Result: "abcdef"

