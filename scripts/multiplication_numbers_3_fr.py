import random

answers = []
ops = ['+', '-', '*', '/']

while True:
    a = random.randint(1,10)
    b = random.randint(1,10)
    op = random.choice(ops)
    if op == '+':
        res = a + b
    elif op == '-':
        a = a + b # On s'assure que 'res' soit
                  # positif
        res = a - b
    elif op == '*':
        res = a * b
    elif op == '/':
        a = a * b
        res = a / b
    print(a,op,b,'= ', end='')
    num = int(input())
    if num == res:
        print('Bravo!')
        answers.append(1)
    else:
        print('Oups, la réponse était',res)
        answers.append(0)
    again = input('Une autre ? [O/n] ')
    if again in ['n', 'N']:
        break

print('Nombre de questions         :', len(answers))
print('Nombre de réponses correctes:', sum(answers))
