# import the library
import pygame
from pygame.locals import *

# initialize game
pygame.init()
screen = pygame.display.set_mode((480, 100))
pygame.display.set_caption('Hello world!')
clock = pygame.time.Clock()
# (...)






# infinite loop
while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    screen.fill(pygame.color(255,255,255))
    # (...)
    pygame.display.update()
    clock.tick(60)
