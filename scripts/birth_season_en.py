from datetime import date

month = int(input('What is your birth month? '))
day = int(input('What is the day of your birth? '))

birth_date = date(2018,month,day)

season = None
if birth_date < date(2018,3,21):
    season = 'winter'
elif birth_date < date(2018,6,22):
    season = 'spring'
elif birth_date < date(2018,9,23):
    season = 'summer'
elif birth_date < date(2018,12,21):
    season = 'fall'
else:
    season = 'winter'

print('You are born in',season)

