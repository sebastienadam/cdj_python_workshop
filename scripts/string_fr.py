a = "Salut le monde!"

a[0]           # 1r caractère de la chaîne
len(a)         # Taille de la chaine
a.index('o')   # Trouve la position de 'o'
a.upper()      # Tout en majuscules
a.lower()      # Tout en minuscules
'-' * 10       # Résultat: "----------"
'abc' + 'def'  # Résultat: "abcdef"

