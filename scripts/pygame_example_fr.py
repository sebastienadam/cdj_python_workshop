# importer la librairie
import pygame
from pygame.locals import *

# initialiser le jeu
pygame.init()
screen = pygame.display.set_mode((480, 100))
pygame.display.set_caption('Salut le monde!')
clock = pygame.time.Clock()
# (...)






# boucle infinie
while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    screen.fill(pygame.color(255,255,255))
    # (...)
    pygame.display.update()
    clock.tick(60)
