#(...)
# infinite loop
while True:
    # (...)
    rect = pygame.Rect(from_x, from_y, width, height)
    # rectangle filled with blue
    pygame.draw.rect(screen, BLUE, rect)
    # rectangle with black borders
    pygame.draw.rect(screen, BLACK, rect, width)
    # (...)
