import string

charset = string.printable

print('Action to do:')
print('1. Encode text')
print('2. Decode text')
action = input('> ')

key = int(input('Key: '))
if action == '1':
    key = -key

print('Text:')
text = input('> ')

transform = ""
for c in text:
    if c in charset:
        transf_idx = charset.index(c)+key
        if transf_idx > len(charset):
            transf_idx = transf_idx % len(charset)
        transform += charset[transf_idx]
    else:
        transform += c

print('Transformed text:')
print(transform)

