ma_liste = [1,2,3,4,5,6]

ma_liste[0]     # 1er élément de la liste
ma_liste[1:5]   # [2, 3, 4, 5]
ma_liste[-4:-2] # [3, 4]
ma_liste[:3]    # [1, 2, 3]
ma_liste[3:]    # [4, 5, 6]

