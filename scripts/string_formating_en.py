num = int(input("Give me a number: "))

# This won't work, you can't concatenate strings and numbers
# message = 'The double of '+num+' is: '+num*2

# This will work
message = 'The double of {} is: {}'.format(num,num*2)

print(message)

