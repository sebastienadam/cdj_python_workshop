import random

answers = []

while True:
    a = random.randint(1,10)
    b = random.randint(1,10)
    res = a * b
    print(a,'x',b,'= ', end='')
    num = int(input())
    if num == res:
        print('Well done!')
        answers.append(1)
    else:
        print('Oups, the answer was',res)
        answers.append(0)
    again = input('Again? [Y/n] ')
    if again in ['n', 'N']:
        break

print('Number of questions   :', len(answers))
print('Number of good answers:', sum(answers))
