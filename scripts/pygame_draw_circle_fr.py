#(...)
# boucle infinie
while True:
    # (...)
    # cercle rempli de bleu
    pygame.draw.circle(screen, BLEU, (centre_x, centre_y), rayon)
    # bords du cercle en noir
    pygame.draw.circle(screen, NOIR, (centre_x, centre_y), rayon, epaiss)
    # (...)
