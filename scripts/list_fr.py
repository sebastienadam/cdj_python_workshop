a = [1, 2, 'a', ['x', 'y']]

a[0]           # 1er élément
a.append(2.5)  # Ajoute une élément à la liste
len(a)         # Taille de la liste
a.remove('a')  # Retire le premier 'a'
a.index(2.5)   # Trouve la position de 2.5
a.sort()       # Trie la liste
sum(a)         # Additionne tous les éléments de la
               # liste. Fonctionne uniquement avec
               # des nombres
