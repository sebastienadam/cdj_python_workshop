import random

secret = random.randint(1,100)
while True:
    num = int(input("Guess the secret number: "))
    if secret == num:
        print("You're a soothsayer!")
        break
    elif num < secret:
        print('To low!')
    else:
        print('To high!')