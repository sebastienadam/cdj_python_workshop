#(...)
# infinite loop
while True:
    # (...)
    # circle filled with blue
    pygame.draw.circle(screen, BLUE, (center_x, center_y), radius)
    # circle with black borders
    pygame.draw.circle(screen, BLACK, (center_x, center_y), radius, width)
    # (...)
