from cipher import cipher
import string
import itertools

pin = input('PIN: ')
ciphered_pin = cipher(pin)

print('Ciphered PIN: {}'.format(ciphered_pin))

for code in itertools.product(string.digits, repeat=6):
    code = ''.join(code)
    ciphered_code = cipher(code)
    if ciphered_code == ciphered_pin:
        print('Found: {} -> {}'.format(ciphered_code,code))
        break

