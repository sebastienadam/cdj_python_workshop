import random

q_a = [
    ["What is the capital of Belgium?","Brussels"],
    ["What is the answer to the Ultimate Question of Life, the Universe, and Everything?","42"],
    ["Do you like Python?","Yes"]
]

question, answer = random.choice(q_a)

print(question)
reply = input("> ")
if reply.lower() == answer.lower():
    print("Well done!")
else:
    print("Oops, the answer is:", answer)
