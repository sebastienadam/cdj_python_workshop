import random

q_a = [
    ["Quelle est la capitale de la Belgique?","Bruxelles"],
    ["Quelle est la réponse à la Question Ultime de la Vie, de l'Univers et du Tout?","42"],
    ["Aimes-tu Python?","Oui"]
]

question, answer = random.choice(q_a)

print(question)
reply = input("> ")
if reply.lower() == answer.lower():
    print("Bravo!")
else:
    print("Oups, la réponse est:", answer)
