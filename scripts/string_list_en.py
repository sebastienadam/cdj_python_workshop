a = "abcdef"

a[0]     # 1st character of the string
a[2:4]   # "cd"

for char in a:
    print(char)

if 'c' in a:
    # (...)

