num = int(input("Donne un nombre: "))

# Ceci ne fonctionnera pas, tu ne peux pas concaténer des chaînes et des nombres
# message = 'Le double de '+num+' est: '+num*2

# Ceci va fonctionner
message = 'Le double de {} est: {}'.format(num,num*2)

print(message)

