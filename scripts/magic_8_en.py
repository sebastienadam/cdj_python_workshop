import random

responses = ['This is your destiny', 'One in two chances', 'Yes absolutely', 'Without a doubt', 'Unlikely', 'Impossible', '42']

while True:
    print("What's your question?")
    question = input()
    if question == '':
        break
    print(random.choice(responses))
