import turtle

while True:
    num_sides = int(input('Nombre de côtés (au moins 3): '))
    if num_sides >= 3:
        break
    print("Le nombre de côtés doit être d'au moins 3!")
if num_sides >= 25:
    turtle.circle(100)
else:
    turtle.speed(75)
    for l in range(num_sides):
        turtle.forward(400/num_sides)
        turtle.left(360/num_sides)
