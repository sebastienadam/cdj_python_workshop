import turtle

while True:
    num_sides = int(input('Number of sides (at least 3): '))
    if num_sides >= 3:
        break
    print('The number of sides must be at least 3!')

if num_sides >= 25:
    turtle.circle(100)
else:
    turtle.speed(75)
    for l in range(num_sides):
        turtle.forward(400/num_sides)
        turtle.left(360/num_sides)
turtle.exitonclick()
