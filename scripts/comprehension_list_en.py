squares = [i**2 for i in range(10)]

# equivalent to
squares = list()
for i in range(10):
    square = i**2
    squares.append(square)

