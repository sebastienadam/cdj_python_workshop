import turtle

turtle.color('red', 'blue')
turtle.speed(25)
turtle.right(60)
turtle.begin_fill()
for l in range(4):
    for j in range(3):
        turtle.forward(256)
        turtle.right(120)
    turtle.right(90)
turtle.end_fill()
turtle.exitonclick()
