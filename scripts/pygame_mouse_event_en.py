# (...)
# infinite loop
while True:
    for event in pygame.event.get():
        if event.type == MOUSEBUTTONDOWN and event.button == 1:
            left_click = True
            mouse_pos = event.pos
        if event.type == MOUSEBUTTONUP and event.button == 1:
            left_click = False
    # (...)
