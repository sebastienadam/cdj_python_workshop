import string

print(string.ascii_lowercase) # a -> z
print(string.ascii_uppercase) # A -> Z
print(string.ascii_letters)   # a -> z + A -> Z
print(string.digits)    # 0 -> 9
print(string.printable) # All printable characters

