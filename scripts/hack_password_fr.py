from cipher import cipher
import string
import itertools

pin = input('PIN: ')
pin_chiffre = cipher(pin)

print('PIN chiffré: {}'.format(pin_chiffre))

for code in itertools.product(string.digits, repeat=6):
    code = ''.join(code)
    code_chiffre = cipher(code)
    if code_chiffre == pin_chiffre:
        print('Found: {} -> {}'.format(code_chiffre,code))
        break

