# (...)
# initialize game
# (...)
pict = pygame.image.load('path/to/picture')
pictrect = pict.get_rect()
# (...)

# infinite loop
while True:
    # (...)
    screen.blit(pict, pictrect)
    # (...)
