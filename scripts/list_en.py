a = [1, 2, 'a', ['x', 'y']]

a[0]           # 1st item of the list
a.append(2.5)  # Add item to the list
len(a)         # Size of the list
a.remove('a')  # Remove first 'a'
a.index(2.5)   # Find the position of 2.5
a.sort()       # Sort the list
sum(a)         # Add all element of the list
               # Works only with numbers
