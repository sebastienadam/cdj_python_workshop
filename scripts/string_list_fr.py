a = "abcdef"

a[0]     # 1er caractère de la chaîne
a[2:4]   # "cd"

for caract in a:
    print(caract)

if 'c' in a:
    # (...)

