#(...)
# boucle infinie
while True:
    # (...)
    rect = pygame.Rect(depuis_x, depuis_y, largeur, hauteur)
    # rectangle rempli de bleu
    pygame.draw.rect(screen, BLEU, rect)
    # bords du rectangle en noir
    pygame.draw.rect(screen, NOIR, rect, epaiss)
    # (...)
