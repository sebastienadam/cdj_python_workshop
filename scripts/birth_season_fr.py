from datetime import date

mois = int(input('Quel est ton mois de naissance? '))
jour = int(input('Quel est ton jour de naissance? '))

date_naiss = date(2018,mois,jour)

saison = None
if date_naiss < date(2018,3,21):
    saison = 'en hivers'
elif date_naiss < date(2018,6,22):
    saison = 'au printemps'
elif date_naiss < date(2018,9,23):
    saison = 'en été'
elif date_naiss < date(2018,12,21):
    saison = 'en automne'
else:
    saison = 'en hivers'

print('Tu es né',saison)

