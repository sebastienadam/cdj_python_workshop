import random

answers = []

while True:
    a = random.randint(1,10)
    b = random.randint(1,10)
    res = a * b
    print(a,'x',b,'= ', end='')
    num = int(input())
    if num == res:
        print('Bravo!')
        answers.append(1)
    else:
        print('Oups, la réponse était',res)
        answers.append(0)
    again = input('Une autre ? [O/n] ')
    if again in ['n', 'N']:
        break

print('Nombre de questions         :', len(answers))
print('Nombre de réponses correctes:', sum(answers))
