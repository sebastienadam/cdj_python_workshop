message = 'hELLo wOrlD!'
print(message.lower())       # hello world!
print(message.upper())       # HELLO WORLD!
print(message.capitalize())  # Hello world!
print(message.title())       # Hello World!
