# (...)
# initialize game
# (...)
font = pygame.font.SysFont('Arial',60)
text = font.render('Hello World!', True, (0,0,0))
textrect = text.get_rect()
# (...)

# infinite loop
while True:
    # (...)
    screen.blit(text, textrect)
    # (...)
