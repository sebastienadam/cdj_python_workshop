my_list = [1,2,3,4,5,6]

my_list[0]     # 1er élément de la liste
my_list[1:5]   # [2, 3, 4, 5]
my_list[-4:-2] # [3, 4]
my_list[:3]    # [1, 2, 3]
my_list[3:]    # [4, 5, 6]

