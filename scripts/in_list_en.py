a = [1, 2, 3, 4, 5]
v = 4

if v in a:
    print(v, "is in the list")
else:
    print(v, "is NOT in the list")
