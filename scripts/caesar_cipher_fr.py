import string

jeu_caract = string.printable

print('Action à exécuter:')
print('1. Chiffrer un texte')
print('2. Déchiffrer un texte')
action = input('> ')

cle = int(input('Clé: '))
if action == '1':
    cle = -cle

print('texte:')
texte = input('> ')

transform = ""
for c in texte:
    if c in jeu_caract:
        transf_idx = jeu_caract.index(c)+cle
        if transf_idx > len(jeu_caract):
            transf_idx = transf_idx % len(jeu_caract)
        transform += jeu_caract[transf_idx]
    else:
        transform += c

print('Texte transformé:')
print(transform)

