import turtle

turtle.setup(width=558, height=528)
turtle.speed(25)
for l in range(1, 120):
    if l > 55 and l < 65:
        continue
    turtle.forward(l)
    turtle.right(30)
turtle.exitonclick()
